import axios from 'axios';
// const rootPath = '127.0.0.1:47420'

axios.defaults.baseURL = 'http://127.0.0.1:47420';

axios.defaults.timeout = 3000;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
axios.defaults.headers["Access-Control-Allow-Origin"] = "*"
axios.defaults.headers["Access-Control-Allow-Methods"] = "*"
axios.defaults.withCredentials = false;
export default new class Send {
    constructor() {

    }

    async get(path, params) {
        let resp = await axios.get(path, {params});
        return resp.data;

    }

    async post(path, body) {
        let resp = await axios.post(path, body);
        return resp.data;

    }
}