import Redis from 'ioredis';
export default new class {
    async connect(config) {
        return new Promise((resolve )=>{
            let redis = new Redis(config)
            redis.on("ready",()=>{
                console.log("连接成功")
            })
            resolve(redis)
        })
    }

}