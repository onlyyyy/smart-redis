# smart-redis

Redis可视化工具，改进RDM和Redisplus的不足之处。

##  一、技术架构
### 前端

Electron + Vue

### 后端
Node.js V16+

## 二、安装

### 代码运行

进入项目目录

```shell
yarn;
npm run dev;
```

跑后端

```shell
npm i;
node main.js;
```


### 打包后的项目运行
只需要执行SmartRedis.exe，该项目会自动拉起backend.exe进行前后端交互。



## 三、功能

软件整体
![img_3.png](img_3.png)

### Key查看


能够方便地查看Redis的key。
![img_2.png](img_2.png)

### Key订阅

这块功能是RedisPlus和RDM的盲区，可以订阅多个Key，进行滚动更新。

![img.png](img.png)

也可以看到一个key的改动日志，这是目前最亮眼的功能。

![img_1.png](img_1.png)

### Redis性能查看
![img_4.png](img_4.png)
可以监控服务器的性能，以及缓存命中情况



## 四、接口

前后端使用restful通信，端口为47420

### 1.key列表


请求:

```js
url="/allKeys"
body={
    config:{
        port: 47420, // Redis port
        host: "127.0.0.1", // Redis host
        password: "pwd",
        db: 0, // Defaults to 0
    },
    pattern:"",
}
```

### 2.检索Key



请求:

```js
url="/allKeys"
body={
    config:{
        port: 47420, // Redis port
        host: "127.0.0.1", // Redis host
        password: "pwd",
        db: 0, // Defaults to 0
    },
    pattern:"*endpoint*",
}
```

同上，只是增加了通配符，返回搜索到的key。

### 3.查看单个Key
```js
url="/singleKey"
body={
    key:"endpoint",
    config:{
        port: 47420, // Redis port
        host: "127.0.0.1", // Redis host
        password: "pwd",
        db: 0, // Defaults to 0
    },
}
```

### 4.订阅

```js
url="/subscribe"
body={
    subscribeList:[
        {
            skeyName:"a",
            skeyType:"string"
        }
    ],
    config:{
        port: 47420, // Redis port
        host: "127.0.0.1", // Redis host
        password: "pwd",
        db: 0, // Defaults to 0
    },
}
```

### 5.服务使用信息

```js
url='/info'
body={
    config:{
        port: 47420, // Redis port
        host: "127.0.0.1", // Redis host
        password: "pwd",
        db: 0, // Defaults to 0
    },
}
```

## 五、技术特点

### 1.项目方面

本项目对比RDM和RedisPlus，具备监控多个key变化的特点，并且提供日志功能。

在实际测试的时候，如果知道key的变化以及具体的时间，那么对于开发者来说有极大的帮助。

### 2.技术方面

- a. 前后端分离，具备很高的扩展性。
- b. 自动拉起后端服务，提供重启和重连功能。
- c. 性能强大，功能浓缩而不臃肿，使用起来会很流畅。

## 六、总结

未来会持续迭代本项目，提供更加完善的功能