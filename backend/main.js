const Redis = require('ioredis');
const fs = require('fs');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const koaStatic = require('koa-static');
const indexRouter = require('./http/index');
const Cors = require('koa-cors');
let Router = require('koa-router');
const Http = require('./http/HttpManager')

let redis = null;

// 分段接受数据
let buffer = '';

// function getConfig() {
//     try {
//         let config = fs.readFileSync('./config.json');
//         config = JSON.parse(config.toString())
//         return config;
//     } catch (error) {
//         console.log("error  ->", error);
//         return null;
//     }
// }


// 根据不同的key读redis
async function readKey(keyType, key) {
    try {
        let value = null;
        if (keyType === 'hash') {
            value = await redis.hgetall(key);
        } else if (keyType === 'string') {
            value = await redis.get(key);
        } else if (keyType === 'zset') {
            value = await redis.zrange(key, 0, -1, "WITHSCORES");
        } else if (keyType === 'list') {
            value = await redis.lrange(key, 0, -1);
        } else if (keyType === 'set') {
            value = await redis.smembers(key)
        }
        return value

    } catch (error) {
        console.log("error ", error);
        return null;
    }


}

function handleResponse(keyInfo, dataType, currentKey) {
    let dataList = [];
    if (dataType === 'hash') {
        for (let keys in keyInfo) {
            dataList.push({
                name: keys,
                value: keyInfo[keys]
            })
        }
    } else if (dataType === 'string') {
        dataList = [{
            name: currentKey,
            value: keyInfo,
        }]
    } else if (dataType === 'set') {
        for (let i = 0; i < keyInfo.length; i++) {
            dataList.push({
                name: "(set)",
                value: keyInfo[i],
            })
        }
    } else if (dataType === 'zset') {
        for (let i = 0; i < keyInfo.length; i++) {
            dataList.push({
                name: "(zset)",
                value: keyInfo[i],
            })
        }
    } else if (dataType === 'list') {
        for (let i = 0; i < keyInfo.length; i++) {
            dataList.push({
                name: "(set)",
                value: keyInfo[i],
            })
        }
    }
    return dataList;
}

async function handleRequest(data) {
    buffer = '';
    data = JSON.parse(data)
    try {
        let type = data.type;
        if (!redis) {
            console.log("重连Redis config->", data.config);
            redis = new Redis(data.config);
        }
        let socketRes = '';
        switch (type) {
            case 'allKeys':
                let keys = await redis.keys('*');
                socketRes = JSON.stringify({
                    type,
                    keys,
                })
                break;
            case "singleKey":
                let key = data.data;
                let keyType = await redis.type(key)
                console.log("keyType ", keyType);

                let ttl = await redis.ttl(key);
                console.log("ttl ", ttl);

                let value = null;
                value = await readKey(keyType, key);
                console.log("value ", value);
                socketRes = JSON.stringify({
                    type: 'singleKey',
                    data: {
                        dataType: keyType,
                        value,
                        key,
                        ttl,
                    }
                })
                break;
            case 'subscribe':
                let list = data.data;
                console.log("list", list);
                let response = []
                for (let i = 0; i < list.length; i++) {
                    let single = list[i];
                    let key = single.skeyName;
                    let keyType = single.skeyType;
                    let res = await readKey(keyType, key);
                    let ttl = await redis.ttl(key);
                    let dataRes = handleResponse(res, keyType, key)
                    response.push({
                        skeyName: key,
                        skeyType: keyType,
                        ttl,
                        skeyInfoData: dataRes,
                    })
                }
                socketRes = JSON.stringify({
                    type: "subscribe",
                    data: response,
                })
                break;
            case 'pattern':
                let pattern = data.data
                let res = await redis.keys(pattern);
                console.log("keys ", res);
                socketRes = JSON.stringify({
                    type,
                    keys: res,
                })
                break;
            default:
                console.error("数据参数错误 type->", type);
                break;
        }
        console.log("应答为  ", socketRes);

    } catch (error) {
        console.error("error->", error)
    }

}

async function requestBuffer(data) {
    buffer += data.toString();
    if (data.length === 65536) {
        console.log("数据还没有接受完")
    }
    await handleRequest(buffer)
}


async function main() {
    bless();
    // let config = getConfig();
    let port = 47420;
    // if (!config) {
    //     port = 47420;
    // } else {
    //     port = config.port;
    // }
    const app = new Koa();
    app.use(bodyParser());
    app.use(Cors());
    let router = new Router();
    router.use('',indexRouter);
    app.use(router.routes());
    app.use(router.allowedMethods());

    app.listen(port, () => {
        console.log("http服务器已启动，127.0.0.1:",port);
    })
    global.http = new Http();

}

function bless() {
    console.log("  _________                      __ __________           .___.__        \n" +
                " /   _____/ _____ _____ ________/  |\\______   \\ ____   __| _/|__| ______\n" +
                " \\_____  \\ /     \\\\__  \\\\_  __ \\   __\\       _// __ \\ / __ | |  |/  ___/\n" +
                " /        \\  Y Y  \\/ __ \\|  | \\/|  | |    |   \\  ___// /_/ | |  |\\___ \\ \n" +
                "/_______  /__|_|  (____  /__|   |__| |____|_  /\\___  >____ | |__/____  >\n" +
                "        \\/      \\/     \\/                   \\/     \\/     \\/         \\/ ")
}

main();