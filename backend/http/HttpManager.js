const Redis = require('ioredis');
const os = require("os");

class HttpManager {
    constructor() {
        this.redis = null;
    }


    // redis处理
    async redisHandler(config) {
        if (!this.redis) {
            console.log("连接或重连redis config->", config);
            this.redis = new Redis(config);
        }
    }


    async allKey(config, pattern = "*") {
        try {
            await this.redisHandler(config);

            return await this.redis.keys(pattern);
        } catch (error) {
            console.log("error ", error);
            this.redis = null;
        }

    }


    // 根据不同的key读redis
    async readKey(keyType, key) {
        try {
            let value = null;
            if (keyType === 'hash') {
                value = await this.redis.hgetall(key);
            } else if (keyType === 'string') {
                value = await this.redis.get(key);
            } else if (keyType === 'zset') {
                value = await this.redis.zrange(key, 0, -1, "WITHSCORES");
            } else if (keyType === 'list') {
                value = await this.redis.lrange(key, 0, -1);
            } else if (keyType === 'set') {
                value = await this.redis.smembers(key)
            }
            return value

        } catch (error) {
            console.log("error ", error);
            return null;
        }


    }

    // 全部的key
    async singleKey(config, key) {
        try {
            await this.redisHandler(config);
            let keyType = await this.redis.type(key)
            console.log("keyType ", keyType);

            let ttl = await this.redis.ttl(key);
            console.log("ttl ", ttl);

            let value = await this.readKey(keyType, key);
            console.log("value ", value);
            return {
                keyType,
                ttl,
                value
            }
        } catch (error) {
            console.log("error ", error);
            this.redis = null;
        }

    }

    // 订阅
    async subscribe(subscribeList, config) {
        await this.redisHandler(config);
        let response = []
        for (let i = 0; i < subscribeList.length; i++) {
            let single = subscribeList[i];
            let key = single.skeyName;
            let keyType = single.skeyType;
            let res = await this.readKey(keyType, key);
            let ttl = await this.redis.ttl(key);
            let dataRes = this.handleResponse(res, keyType, key)
            response.push({
                skeyName: key,
                skeyType: keyType,
                ttl,
                skeyInfoData: dataRes,
            })
        }
        return response;

    }


    handleResponse(keyInfo, dataType, currentKey) {
        let dataList = [];
        if (dataType === 'hash') {
            for (let keys in keyInfo) {
                dataList.push({
                    name: keys,
                    value: keyInfo[keys]
                })
            }
        } else if (dataType === 'string') {
            dataList = [{
                name: currentKey,
                value: keyInfo,
            }]
        } else if (dataType === 'set') {
            for (let i = 0; i < keyInfo.length; i++) {
                dataList.push({
                    name: "(set)",
                    value: keyInfo[i],
                })
            }
        } else if (dataType === 'zset') {
            for (let i = 0; i < keyInfo.length; i++) {
                dataList.push({
                    name: "(zset)",
                    value: keyInfo[i],
                })
            }
        } else if (dataType === 'list') {
            for (let i = 0; i < keyInfo.length; i++) {
                dataList.push({
                    name: "(set)",
                    value: keyInfo[i],
                })
            }
        }
        return dataList;
    }

    // 执行命令
    async info(config) {
        await this.redisHandler(config);
        let res = await this.redis.info();
        res = this.parseInfo(res)
        return res;
    }

    // 解析redis的info 原始数据是按行算的
    parseInfo(infoData) {
        let dataLine = infoData.split(os.EOL);
        let res = []
        for (let i = 0; i < dataLine.length; i++) {
            let single = dataLine[i];
            if (single[0] === '#') {
                continue;
            }
            let realData = single.split(':')
            if (realData.length > 2) {
                let combine = ''
                for (let j = 1; j < realData.length; j++) {
                    if (j === 1) {
                        combine += realData[j]
                    } else {
                        combine += ":" + realData[j]
                    }

                }
                realData[1] = combine;
            }
            if (!realData[0] || !realData[1]) {
                continue;
            }
            res.push({
                key: realData[0],
                data: realData[1]
            })
        }
        return res;
    }

}

module.exports = HttpManager;
