const router = require('koa-router')();
router.get('/' ,async(ctx)=>{

    ctx.body = "hello world";

})

router.post('/allKeys',async(ctx)=>{
    let body = ctx.request.body;
    let {config,pattern} = body;
    let res = await http.allKey(config,pattern);
    ctx.body = {
        code:200,
        data:{
            keys:res
        }
    };
})

router.post('/singleKey',async(ctx)=>{
    let body = ctx.request.body;
    let {config,key} = body;
    let res = await http.singleKey(config,key);
    ctx.body = {
        code:200,
        data:res
    };
})

router.post('/subscribe',async(ctx)=>{
    let body = ctx.request.body;
    let {subscribeList,config} = body;
    console.log("subscribeList ",subscribeList);
    let res =await http.subscribe(subscribeList,config);
    ctx.body = {
        code:200,
        data:res
    };
})

router.post("/redisInfo",async(ctx)=>{
    let body = ctx.request.body;
    let {config} = body
    let res = await http.info(config);
    ctx.body = {
        code:200,
        data:{
            info:res
        }
    };
})

module.exports = router.routes();